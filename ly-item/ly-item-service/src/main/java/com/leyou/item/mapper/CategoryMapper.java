package com.leyou.item.mapper;

import com.leyou.item.pojo.Category;
import tk.mybatis.mapper.additional.idlist.SelectByIdListMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author HongBo Wang
 * @data 2018/6/23 21:32
 */
public interface CategoryMapper extends Mapper<Category>,SelectByIdListMapper<Category, Long> {


}
