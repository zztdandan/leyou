package com.leyou.item.mapper;

import com.leyou.item.pojo.Brand;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.additional.idlist.SelectByIdListMapper;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @author HongBo Wang
 * @data 2018/6/24 10:57
 */
public interface BrandMapper extends Mapper<Brand>,SelectByIdListMapper<Brand,Long> {
    /**新增商品分类和品牌中间表数据
     * @param cid
     * @param bid
     * @return
     */
    @Insert("insert into tb_category_brand (category_id,brand_id) values (#{cid},#{bid})")
    int insertCategoryBrand(@Param("cid") Long cid, @Param("bid") Long bid);

    /**根据分类查询品牌信息(有中间表,需要自定义sql)
     * @param cid
     * @return
     */
    @Select("select b.* from tb_category_brand cb left join tb_brand b on cb.brand_id = b.id where cb.category_id = #{cid}")
    List<Brand> queryByCid(Long cid);
}
