package com.leyou.item.mapper;

import com.leyou.item.pojo.Stock;
import tk.mybatis.mapper.additional.idlist.DeleteByIdListMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * @author HongBo Wang
 * @data 2018/6/28 20:26
 */
public interface StockMapper extends Mapper<Stock>,DeleteByIdListMapper<Stock,Long> {
}
