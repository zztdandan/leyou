package com.leyou.auth;

import com.leyou.auth.entity.UserInfo;
import com.leyou.auth.utils.JwtUtils;
import com.leyou.auth.utils.RsaUtils;
import org.junit.Before;
import org.junit.Test;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * @author HongBo Wang
 * @data 2018/7/18 10:48
 */
public class JwtTest {
    private static final String pubKeyPath = "D:\\heima30\\rsa\\rsa.pub";

    private static final String priKeyPath = "D:\\heima30\\rsa\\rsa.pri";

    private PublicKey publicKey;

    private PrivateKey privateKey;

    @Test
    public void testRsa() throws Exception {
        RsaUtils.generateKey(pubKeyPath, priKeyPath, "234");
    }

    @Before
    public void testGetRsa() throws Exception {
        this.publicKey = RsaUtils.getPublicKey(pubKeyPath);
        this.privateKey = RsaUtils.getPrivateKey(priKeyPath);
    }

    @Test
    public void testGenerateToken() throws Exception {
        // 生成token
        String token = JwtUtils.generateToken(new UserInfo(20L, "jack"), privateKey, 5);
        System.out.println("token = " + token);
    }

    @Test
    public void testParseToken() throws Exception {
        String token = "eyJhbGciOiJSUzI1NiJ9.eyJpZCI6MjAsInVzZXJuYW1lIjoiamFjayIsImV4cCI6MTUzMTg4MzA5NX0.VbswGv8aARkVqKeBHxLBd03LOLOMh6P4Oswc56ZoxApp7kli3bCm5FdewWVMZJkik4QsIvCbY5_nvc4ISa5yHKZwH1omlt-wbi4fljbBEwVyoYiWowaKAMNWoOVcOuh8R2DcQIN9tuIL-IXnGbmr2D9V_N981vfdjUEJI92fR_s";

        // 解析token
        UserInfo user = JwtUtils.getInfoFromToken(token, publicKey);
        System.out.println("id: " + user.getId());
        System.out.println("userName: " + user.getUsername());
    }
}
